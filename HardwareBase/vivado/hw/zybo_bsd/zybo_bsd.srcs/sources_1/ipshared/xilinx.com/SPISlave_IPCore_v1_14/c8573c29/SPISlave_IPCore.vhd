----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Servio Paguada
-- 
-- Create Date: 01/23/2015 12:59:48 AM
-- Design Name: 
-- Module Name: SPISlave_IPCore - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SPISlave_IPCore is
    generic (--For SPI modes and others
    cpol    : STD_LOGIC := '0';  --spi clock polarity mode, idle in low by default
    cpha    : STD_LOGIC := '0';  --spi clock phase mode, data will be sampled on the first edge by default
    d_width : INTEGER := 24);     --data width in bits
    Port (SCLK               : in  STD_LOGIC;
          MISO               : out STD_LOGIC;
          MOSI               : in  STD_LOGIC;
          SS_N               : in  STD_LOGIC;
          R_out              : out STD_LOGIC_VECTOR(7 downto 0);
          G_out              : out STD_LOGIC_VECTOR(7 downto 0);
          B_out              : out STD_LOGIC_VECTOR(7 downto 0);
          G_in               : in  STD_LOGIC_VECTOR(7 downto 0));
          
end SPISlave_IPCore;

architecture Behavioral of SPISlave_IPCore is

    signal mode              : STD_LOGIC;  --groups modes by clock polarity relation to data
    signal clk               : STD_LOGIC;                                                 --clock
    signal rx_shift_register : STD_LOGIC_VECTOR(d_width - 1 downto 0) := (others => '0'); --receiver buffer
    signal tx_shift_register : STD_LOGIC_VECTOR(7 downto 0) := (others => '0'); --trasmiter buffer
    
    TYPE POSSIBLE_STATES IS (waiting, shifting);
    signal state : POSSIBLE_STATES;
begin

--adjust clock so writes are on rising edge and reads on falling edge
--'1' for modes that write on rising edge
mode <= cpol xor cpha;
with mode select 
clk <= SCLK when '1', 
       not SCLK when others;
    
--assign values for R2GrayValue component in parallel
B_out <= rx_shift_register(23 downto 16);

G_out <= rx_shift_register(15 downto 8);
                     
R_out <= rx_shift_register(7 downto 0);
                 
--assign data from the buffer_out to tx_shit_register, in orden to keep the data for the MISO
tx_shift_register <= G_in;

--process for collect data from MOSI
process(clk, SS_N)
    variable shift_counter  : integer := 0;
    variable bit_count      : integer := 0;    
begin

   if(SS_N = '1') then --reset signals
        rx_shift_register <= (others => '0');
        state <= waiting;
        shift_counter := 0;
        bit_count := 0;
   elsif(clk'event and clk='0') then
        case state is
            when waiting =>
                shift_counter := 0;
                rx_shift_register <= rx_shift_register(d_width - 2 downto 0) & MOSI;
                MISO <= '0';
                bit_count := bit_count + 1;
                if(bit_count >= 24) then
                    state <= shifting;
                else
                    state <= waiting;
                end if;
            when shifting =>
                rx_shift_register <= rx_shift_register;
                shift_counter := shift_counter + 1;
                MISO <= tx_shift_register(8 - shift_counter);
                --tx_shift_register <= tx_shift_register(6 downto 0) & '0';
                if (shift_counter >= 8) then
                    state <= waiting;
                    
                    rx_shift_register <= (others => '0');--new things
                    shift_counter := 0;--new things
                    bit_count := 0;--new things
                else
                    state <= shifting;
                end if;
        end case;
    end if;
    
end process;

end Behavioral;
