----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/11/2015 02:37:16 AM
-- Design Name: 
-- Module Name: test_pkg - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

--Package declaration for the above program

package test_pkg is
 --function declaration.
function  divide  (a : UNSIGNED; b : UNSIGNED) return UNSIGNED;
end test_pkg;   --end of package.

package body test_pkg is  --start of package body
--definition of function
function  divide  (a : UNSIGNED; b : UNSIGNED) return UNSIGNED is
    variable a1 : unsigned(a'length-1 downto 0):=a;
    variable b1 : unsigned(b'length-1 downto 0):=b;
    variable p1 : unsigned(b'length downto 0):= (others => '0');
    variable i : integer:=0;

begin
for i in 0 to b'length-1 loop
    p1(b'length-1 downto 1) := p1(b'length-2 downto 0);
    p1(0) := a1(a'length-1);
    a1(a'length-1 downto 1) := a1(a'length-2 downto 0);
    p1 := p1-b1;
    if(p1(b'length-1) ='1') then
        a1(0) :='0';
        p1 := p1+b1;
    else
        a1(0) :='1';
    end if;
end loop;

return a1;
end divide;
--end function
end test_pkg;  --end of the package body