-- (c) Copyright 1995-2015 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: xilinx.com:ip:SPISlave_IPCore:1.14
-- IP Revision: 1

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY system_SPISlave_IPCore_0_0 IS
  PORT (
    SCLK : IN STD_LOGIC;
    MISO : OUT STD_LOGIC;
    MOSI : IN STD_LOGIC;
    SS_N : IN STD_LOGIC;
    R_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    G_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    B_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    G_in : IN STD_LOGIC_VECTOR(7 DOWNTO 0)
  );
END system_SPISlave_IPCore_0_0;

ARCHITECTURE system_SPISlave_IPCore_0_0_arch OF system_SPISlave_IPCore_0_0 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : string;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF system_SPISlave_IPCore_0_0_arch: ARCHITECTURE IS "yes";

  COMPONENT SPISlave_IPCore IS
    GENERIC (
      cpol : STD_LOGIC;
      cpha : STD_LOGIC;
      d_width : INTEGER
    );
    PORT (
      SCLK : IN STD_LOGIC;
      MISO : OUT STD_LOGIC;
      MOSI : IN STD_LOGIC;
      SS_N : IN STD_LOGIC;
      R_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      G_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      B_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      G_in : IN STD_LOGIC_VECTOR(7 DOWNTO 0)
    );
  END COMPONENT SPISlave_IPCore;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF system_SPISlave_IPCore_0_0_arch: ARCHITECTURE IS "SPISlave_IPCore,Vivado 2014.2";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF system_SPISlave_IPCore_0_0_arch : ARCHITECTURE IS "system_SPISlave_IPCore_0_0,SPISlave_IPCore,{}";
  ATTRIBUTE CORE_GENERATION_INFO : STRING;
  ATTRIBUTE CORE_GENERATION_INFO OF system_SPISlave_IPCore_0_0_arch: ARCHITECTURE IS "system_SPISlave_IPCore_0_0,SPISlave_IPCore,{x_ipProduct=Vivado 2014.2,x_ipVendor=xilinx.com,x_ipLibrary=ip,x_ipName=SPISlave_IPCore,x_ipVersion=1.14,x_ipCoreRevision=1,x_ipLanguage=VERILOG,cpol=0,cpha=0,d_width=24}";
BEGIN
  U0 : SPISlave_IPCore
    GENERIC MAP (
      cpol => '0',
      cpha => '0',
      d_width => 24
    )
    PORT MAP (
      SCLK => SCLK,
      MISO => MISO,
      MOSI => MOSI,
      SS_N => SS_N,
      R_out => R_out,
      G_out => G_out,
      B_out => B_out,
      G_in => G_in
    );
END system_SPISlave_IPCore_0_0_arch;
